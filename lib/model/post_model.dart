/*
 * Copyright by Aphelia Innovation,This whole code is developed  and managed and redeveloped by parmeet singh,with authority of Aphelia.
 */

class PostModel{

String title;
String image;
String content;
List<String> files;
String date;

PostModel({this.title, this.image, this.content, this.files, this.date});
}